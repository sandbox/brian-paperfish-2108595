<?php

include_once(drupal_get_path('module', 'webform') . '/includes/webform.report.inc');

function webform_extended_table_webform_results_table($node, $pager_count = 0) {
	$request = strtolower($_SERVER['REQUEST_METHOD']);

	if($request == 'get') {
		if(!isset($_GET['_getdata'])) {
			drupal_add_js('sites/all/libraries/extjs/ext-all.js');
			drupal_add_css('sites/all/libraries/extjs/resources/css/ext-all.css','module','all',true);
			drupal_add_css(drupal_get_path('module', 'webform_extended_table') . '/css/style.css','module','all',true);
			$output = theme('webform_extended_table_webform_results_table', array('node' => $node, 'components' => $node->webform['components'], 'pager_count' => $pager_count));
		} else if($_GET['_getdata'] == 'excel') {
			header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public");
			header("Content-Type: text/csv; charset=utf8");
			header("Content-Disposition: attachment; filename=submissions_node_" . $node->nid . ".csv");
			$result = webform_extended_table_webform_results($node,10000,true);
			$result = transformCsv($result,$node,10000);
			header("Content-length: " . (strlen($result)+3));
			echo "\xef\xbb\xbf";
			echo $result;
			drupal_exit(0);
		} else {
			// Return all the submissions for the node as JSON.
			if(isset($_GET['limit'])) {
				$pager_count = intval($_GET['limit']);
			}
			$result = webform_extended_table_webform_results($node,$pager_count,false);
			drupal_json_output($result);
			drupal_exit(0);
		}
	} else if($request == 'delete') {
		$data = file_get_contents('php://input');
		$data = json_decode($data);
		$submission = webform_get_submission($node->nid,$data->sid);
		if(!isset($submission)) {
			drupal_json_output(array('success'=>'false','message'=>'Could not retrieve submission (' . $data->sid . ').'));
			drupal_exit(0);
		}
		if(webform_submission_access($node, $submission, 'delete')) {
			webform_submission_delete($node,$submission);
			drupal_json_output(array('success'=>'true'));
			drupal_exit(0);
		} else {
			drupal_json_output(array('success'=>'false','message'=>'You do not have access to delete this submission (' . $data->sid . ').'));
			drupal_exit(0);
		}
	} else if($request == 'put') {
		$data = file_get_contents('php://input');
		$data = json_decode($data);
		$submission = webform_get_submission($node->nid,$data->sid);
		if(!isset($submission)) {
			drupal_json_output(array('success'=>'false','message'=>'Could not retrieve submission (' . $data->sid . ').'));
			drupal_exit(0);
		}
		if(webform_submission_access($node, $submission, 'edit')) {
			updateSubmission($submission,$node,$data);
			webform_submission_update($node, $submission);
			drupal_json_output(array('success'=>'true'));
			drupal_exit(0);
		} else {
			drupal_json_output(array('success'=>'false','message'=>'You do not have access to edit this submission (' . $data->sid . ').'));
			drupal_exit(0);
		}
	} else if($request == 'post') {
		$data = file_get_contents('php://input');
		$data = json_decode($data);
		insert($data);
		exit;
	}

	return $output;
}

function updateSubmission(&$submission, $node, $data) {
	foreach($data as $formKey=>$value) {
		if($formKey != 'sid') {
			foreach($node->webform['components'] as $component) {
				if($formKey == $component['form_key']) {
					if($component['type'] == 'select') {
						$values = explode(',',$value);
						for($i=0; $i<count($values); $i++) {
							if($values[$i] === 'false' && isset($component['extra']) && isset($component['extra']['items']) && count($component['extra']['items']) == 1 && isset($component['extra']) && isset($component['extra']['multiple']) && $component['extra']['multiple']) {
								$values[$i] = '';
							}
						}
					} else {
						$values = array($value);
					}
					$submission->data[$component['cid']] = $values;
				}
			}
		}
	}
}

function transformCsv($result, $node, $count) {
	$a = array();
	if($count < $result['total']) {
		$a[] = array('WARNING: Maximum results of ' + $count + ' submissions exceeded! This report contains only the most recent ' + $count + ' submissions. Try filtering data to reduce number of submissions.');
	}
	$a[] = array('Report Date: ' . date( 'D d M Y H:i:s' ) . (isset($_GET['filter']) ? (', filtered on ' . $_GET['filter']) : ', no filters'));
	$header = theme('webform_results_table_header', array('node' => $node));

	$component_headers = array();

    // Generate header data.
	foreach ($node->webform['components'] as $component) {
		$submission_output = webform_component_invoke($component['type'], 'table', $component, null);
		if ($submission_output !== NULL) {
			$component_headers[] = array('data' => $component['name'], 'field' => $component['form_key'], 'type' => $component['type'] , 'required' => $component['required'], 'options' => isset($component['extra']) && isset($component['extra']['items']) ? $component['extra']['items'] : array(), 'multiple' => isset($component['extra']) && isset($component['extra']['multiple']) ? ($component['extra']['multiple'] ? true : false) : false);
		}
	}

	$columnToFieldName = array();
	if (!empty($component_headers)) {
		$header = array_merge($header, $component_headers);
		$cnt = -1;
		$col = count($a);
		foreach($header as $column) {
			++$cnt;
			if($column['field'] == 'sid' && $cnt == 0) {
				$a[$col][] = 'SID';
			} else {
				$a[$col][] = $column['data'];
			}
			if($column['field'] == 'name' && $cnt == 2) {
				$columnToFieldName[] = '_uname';
			} else {
				$columnToFieldName[] = $column['field'];
			}
		}
	}
	
	foreach($result['results'] as $submission) {
		$row = count($a);
		$col = -1;
		foreach($header as $column) {
			++$col;
			if(isset($submission[$columnToFieldName[$col]])) {
				if($columnToFieldName[$col] == '_uname' && $submission[$columnToFieldName[$col]] == '') {
					$a[$row][] = 'Anonymous';
				} else {
					$a[$row][] = $submission[$columnToFieldName[$col]];
				}
			} else {
				$a[$row][] = '';

			}
		}
	}
	
	$csv_string = '';
	for($i=0; $i<count($a); $i++) {
		for($j=0; $j<count($a[$i]); $j++) {
			if($j > 0) $csv_string .= ',';
			$csv_string .= '"' . str_replace('"','""',$a[$i][$j]) . '"';
		}
		$csv_string .= "\r\n";
	}
	return $csv_string;
}

function webform_extended_table_webform_results($node,$pager_count,$forDisplay = false) {
	if (isset($_GET['results']) && is_numeric($_GET['results'])) {
		$pager_count = $_GET['results'];
	}
	if(isset($_GET['page'])) {
		$_GET['page'] = intval($_GET['page']) - 1;
	}
	if(isset($_GET['sort'])) {
		$sortObj = json_decode($_GET['sort']);
		$sort = $sortObj[0]->property;
		$order = $sortObj[0]->direction;
		$_GET['order'] = $sort == '_uname' ? 'User' : ($sort == 'remote_addr' ? 'IP Address' : ($sort == 'submitted' ? 'Submitted' : 'sid'));
		$_GET['sort'] = strtolower($order);
	}

	$header = theme('webform_results_table_header', array('node' => $node));
	$fieldToCidMap = array();
	foreach ($node->webform['components'] as $component) {
		$submission_output = webform_component_invoke($component['type'], 'table', $component, null);
		if ($submission_output !== NULL) {
			$component_headers[] = array('data' => $component['name'], 'field' => $component['form_key'], 'cid' => $component['cid']);
			$formKeyToCidMap[$component['form_key']] = $component['cid'];
		}
	}
	
	$submissions = webform_get_submissions($node->nid, $header, $pager_count);
	$total_count = webform_get_submission_count($node->nid);
	if (!empty($component_headers)) {
		$header = array_merge($header, $component_headers);
		$fields = array();
		foreach($header as $column) {
			$found = false;
			foreach ($node->webform['components'] as $component) { 
				if($component['form_key'] == $column['field']) {
					$fields[] = array('field' => $column['field'], 'cid' => $column['cid'], 'component' => $component);
					$found = true;
				}
			}
			if(!$found) {
				$fields[] = array('field' => $column['field'], 'cid' => isset($column['cid']) ? $column['cid'] : null );
			}
		}
		$results = array();
		foreach($submissions as $sid=>$submission) {
			$result = array();
			$cnt = -1;
			foreach($fields as $field) {
				++$cnt;
				if($field['field'] == 'sid' && $cnt == 0) {
					$result[$field['field']] = $sid;
				} else if($field['field'] == 'submitted' && $cnt == 1) {
					$result[$field['field']] = format_date($submission->submitted, 'short');
				} else if($field['field'] == 'name' && $cnt == 2) {
					if($forDisplay) {
						$result['_uname'] = $submission->name;
					} else {
						$result['_uname'] = str_replace('<a ','<a target="_blank" ',theme('username', array('account' => $submission)));
					}
				} else if($field['field'] == 'remote_addr' && $cnt == 3) {
					$result[$field['field']] = $submission->remote_addr;
				} else {
					if($forDisplay) {
						$data = isset($submission->data[$field['component']['cid']]) ? $submission->data[$field['component']['cid']] : NULL;
						$submission_output = webform_component_invoke($field['component']['type'], 'table', $field['component'], $data);
						if(isset($field['component']) && $field['component']['type'] == 'file' && $submission_output != '') {
							$submission_output = trim($submission_output);
							$pos = strpos($submission_output,'>')+1;
							$submission_output = substr($submission_output,$pos,strpos($submission_output,'<',$pos) - $pos);
						} else if(isset($submission->data[$field['cid']]) && count($submission->data[$field['cid']]) > 1) {
							$submission_output = str_replace('<br />',";",$submission_output);
						} else if(isset($submission->data[$field['cid']]) && count($submission->data[$field['cid']]) == 1 && isset($field['component']) && isset($field['component']['extra']) && isset($field['component']['extra']['items']) && count($field['component']['extra']['items']) == 1 && isset($field['component']['extra']) && isset($field['component']['extra']['multiple']) && $field['component']['extra']['multiple']) {
							if($submission->data[$field['cid']] == 'true' || $submission->data[$field['cid']] == true) {
								$submission_output = 'yes';
							} else {
								$submission_output = 'no';
							}
						}
						$result[$field['field']] = $submission_output;
					} else if(isset($submission->data[$field['cid']]) && count($submission->data[$field['cid']]) > 1) {
						$values = array();
						foreach($submission->data[$field['cid']] as $value) {
							$values[] = $value;
						}
						$result[$field['field']] = $values;
					} else {
						if(isset($field['component']) && $field['component']['type'] == 'file') {
							$data = isset($submission->data[$field['component']['cid']]) ? $submission->data[$field['component']['cid']] : NULL;
							$submission_output = webform_component_invoke($field['component']['type'], 'table', $field['component'], $data);
							if ($submission_output !== NULL) {
								$result[$field['field']] = str_replace('<a ','<a target="_blank" ',$submission_output);
							} else {
								$result[$field['field']] = $submission->data[$field['cid']][0];
							}
						} else {
							if(isset($submission->data[$field['cid']])) {
								$result[$field['field']] = $submission->data[$field['cid']][0];
							}
						}
					}
				}
			}
			$results[] = $result;
		}
	}
	return array('success' => true,'total' => $total_count,'results' => $results);

}

function theme_webform_extended_table_webform_results_table($variables) {
	drupal_add_library('webform', 'admin');

	$node = $variables['node'];
	$components = $variables['components'];
	$pager_count = $variables['pager_count'];
	
	$possiblePagerCounts = array(50,100,150,200,500);
	if(!in_array($pager_count,$possiblePagerCounts)) {
		$possiblePagerCounts[] = $pager_count;
	}
	sort($possiblePagerCounts);

	$header = array();
	$rows = array();
	$cell = array();

	$header = theme('webform_results_table_header', array('node' => $node));
	$header[2]['field'] = '_uname';  // Don't confuse with other fields with name form_key

	$component_headers = array();

    // Generate header data.
	foreach ($node->webform['components'] as $component) {
		$submission_output = webform_component_invoke($component['type'], 'table', $component, null);
		if ($submission_output !== NULL) {
			$component_headers[] = array('data' => $component['name'], 'field' => $component['form_key'], 'type' => $component['type'] , 'required' => $component['required'], 'options' => isset($component['extra']) && isset($component['extra']['items']) ? $component['extra']['items'] : array(), 'multiple' => isset($component['extra']) && isset($component['extra']['multiple']) ? ($component['extra']['multiple'] ? true : false) : false);
		}
	}

	if (!empty($component_headers)) {
		$header = array_merge($header, $component_headers);
		$editAccess = _webform_extended_table_webform_submission_access($node,null,'edit');
		$deleteAccess = _webform_extended_table_webform_submission_access($node,null,'delete');
		$columns = array(array('text' => '&nbsp;', 'dataIndex' => '_selector', 'xtype' => 'checkcolumn', 'width' => 40, 'listeners' => array('checkchange' => 'deleteSelectionListener'), 'sortable' => false));
		$fields = array(array('name' => '_selector', 'type' => 'bool', 'persist' => false));
		if(!$deleteAccess) {
			$columns[0]['hidden'] = true;
		}
		$cnt = 0;
	$js = " 
Ext.onReady(function(){
	var sidTemplate = new Ext.Template('(<a href=\"" . url('node/' . $node->nid . '/submission/') . '{0}' . "\">view</a>)" . ($editAccess ? " (<a href=\"" . url('node/' . $node->nid . '/submission/') . '{0}/edit' . "\">edit</a>)" : "") . "',{compiled: true});

	var sidRenderer = function(sid) {
		return '<span class=\"x-readonly\">' + sid + '</span>&nbsp;&nbsp;&nbsp;' + sidTemplate.apply([sid]);
	}

	var readOnlyRenderer = function(val) {
		return '<span class=\"x-readonly\">' + val + '</span>';
	}

	var readOnlyWithEncodeRenderer = function(val) {
		return '<span class=\"x-readonly\">' + Ext.util.Format.htmlEncode(val) + '</span>';
	}

	";

		$comboRendererWritten = false;
		$multiselectListenerWritten = false;
		$validateDates = array();
		$validateTimes = array();
		$label = '';
		$value = '';
		foreach($header as $column) {
			++$cnt;
			if($cnt < 5) {
				if($cnt == 1) {
					$columns[] = array('text' => 'SID', 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'sidRenderer');
				} else if($cnt == 2) {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'readOnlyRenderer');
				} else if($cnt == 3) {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => '_uname', 'renderer' => 'readOnlyRenderer');
				} else {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'readOnlyRenderer');
				}
			} else {
				if($column['type'] == 'select') {
					if(is_string($column['options'])) {
						$jsStore = "
	var store_" . $cnt . " = Ext.create('Ext.data.Store', {
		fields: ['id','name'],
		data: [";
						$opts = explode("\n",$column['options']);
						$first = true;
						$optionCount = 0;
						if(!$column['multiple']) {
							$jsStore .= "
			{id: '', name: '<em>none</em>'}";
							$first = false;
						}
						foreach($opts as $opt) {
							$opt = trim($opt);
							if(!empty($opt)) {
								$pos = strpos($opt,'|');
								if($pos) {
									if(!$first) $jsStore .= ",";
									$label = substr($opt,$pos+1);
									$value = substr($opt,0,$pos);
									$optionCount++;
									$jsStore .= "
			{id: '" . str_replace("'","\\'",$value) . "', name: '" . str_replace("'","\\'",$label) . "'}";
									$first = false;
								}
							}
						}
						$jsStore .= "
		]
	});";
					}
					if($optionCount == 1 && $column['multiple'] && ($value === true || $value === 'true') ) {
						if($deleteAccess) {
							$columns[] = array('text' => htmlspecialchars($column['data']), 'xtype' => 'checkcolumn', 'width' => 120, 'dataIndex' => $column['field'], 'sortable' => false);
						} else {
							$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'sortable' => false);
						}
					} else {
						if(!$comboRendererWritten) {
							$js .= "
	var comboRenderer = function(store) {
		return function(val){
			if(val.indexOf(',') > -1) {
				var vals = val.split(',');
				var display = '';
				for(var i=0; i<vals.length; i++) {
					var index = store.findExact('id',vals[i]); 
					if (index != -1){
						var rs = store.getAt(index).data; 
						if(i==0) {
							display += Ext.util.Format.htmlEncode(rs.name);
						} else {
							display += ', ' + Ext.util.Format.htmlEncode(rs.name);
						}
					}
				}
				return display;
			} else {
				if(val == '') {
					return '';
				}
				var index = store.findExact('id',val); 
				if (index != -1){
					var rs = store.getAt(index).data; 
					return Ext.util.Format.htmlEncode(rs.name); 
				}
			}
		}	
	};

	var dateRenderer = function(val){
		if(val == '') return '';
		return val.substring(5,7) + '/' + val.substring(8,10) + '/' + val.substring(0,4);
	};
	
	var recordsToDelete = {};
	var recordsToDeleteSize = 0;
	var deleteSelectionListener = function(cb,row,checked){
		if(checked) {
			var record = store.getAt(row);
			recordsToDelete[record.getId()] = record;
			++recordsToDeleteSize;
		} else {
			var record = store.getAt(row);
			delete recordsToDelete[record.getId()];
			--recordsToDeleteSize;
		}
		updateDeletes();
	};
	var updateDeletes = function() {
		if(recordsToDeleteSize > 0) {
			Ext.getCmp('deleteButton').enable();
		} else {
			Ext.getCmp('deleteButton').disable();
		}
	}
	var assessDelete = function() {
		recordsToDelete = {};
		recordsToDeleteSize = 0;
		var count = store.getCount();
		for(var i=0; i<count; i++) {
			var record = store.getAt(i);
			if(record.get('_selector')) {
				recordsToDelete[record.getId()] = record;
				++recordsToDeleteSize;
			}
		}
		updateDeletes();
	}
	
	var comboEmptyCheck = function(c,r) {
		if(r[0].data.id == '') {
			c.clearValue();
		}
	}
	
	var comboEmptyCheckFocus = function(c) {
		if(c.getValue() == '') {
			c.clearValue();
		}
	}";
							$comboRendererWritten = true;
						}
						if($column['multiple'] && !$multiselectListenerWritten) {
							$js .= "
	var multiselectListener = function(field) {
		return function(c){
			var value = grid.getSelectionModel().selected.first().data[field];
			if(value && value != '') {
				c.setValue(value.split(','));
			}
		}
	};";
							$multiselectListenerWritten = true;	
						}
						$js .= $jsStore;
						if($column['multiple']) {
							$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'comboRenderer_' . $cnt);
							if($editAccess) $columns[count($columns)-1]['editor'] = array('xtype' => 'combobox', 'store' => 'store_' . $cnt, 'forceSelection' => true, 'queryMode' => 'local', 'displayField' => 'name', 'valueField' => 'id', 'editable' => false, 'multiSelect' => $column['multiple'], 'listeners' => 'multiselectListener_' . $column['field'], 'sortable' => false);
						} else {
							$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'comboRenderer_' . $cnt);
							if($editAccess) $columns[count($columns)-1]['editor'] = array('xtype' => 'combobox', 'store' => 'store_' . $cnt, 'forceSelection' => true, 'queryMode' => 'local', 'displayField' => 'name', 'valueField' => 'id', 'editable' => false, 'listeners' => 'comboEmptyCheck', 'sortable' => false);
						}
					}
				} else if($column['type'] == 'date') {
					$validateDates[] = $column['field'];
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'dateRenderer', 'sortable' => false);
					if($editAccess) $columns[count($columns)-1]['editor'] = array('xtype' => 'datefield', 'format' => 'Y-m-d');
				} else if($column['type'] == 'time') {
					$validateTimes[] = $column['field'];
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'sortable' => false);
					if($editAccess) $columns[count($columns)-1]['editor'] = array('xtype' => 'timefield', 'format' => 'H:i:s');
				} else if($column['type'] == 'grid') {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'readOnlyWithEncodeRenderer', 'sortable' => false);
				} else if($column['type'] == 'mapping') {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'readOnlyWithEncodeRenderer', 'sortable' => false);
				} else if($column['type'] == 'textarea') {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'htmlRenderer', 'sortable' => false);
					if($editAccess) $columns[count($columns)-1]['editor'] = 'textarea';
				} else if($column['type'] == 'number') {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'sortable' => false);
					if($editAccess) $columns[count($columns)-1]['editor'] = 'numberfield';
				} else if($column['type'] == 'file') {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'sortable' => false);
				} else {
					$columns[] = array('text' => htmlspecialchars($column['data']), 'width' => 120, 'dataIndex' => $column['field'], 'renderer' => 'htmlRenderer', 'sortable' => false);
					if($editAccess) $columns[count($columns)-1]['editor'] = 'textfield';
				}
			}
			if($cnt == 1) {
				$fields[] = array('name' => $column['field'], 'type' => 'int');
			} else {
				$fields[] = array('name' => $column['field'], 'type' => 'string');
			}
		}
	

	$js .= "

	Ext.define('submission', {
		extend: 'Ext.data.Model',
		idProperty : 'sid',
		fields: " . json_encode($fields) . "
	});

	var store = Ext.create('Ext.data.Store', {
		pageSize: " . $pager_count . ",
		autoLoad: {params: {start: 0, limit: " . $pager_count . "}},
		autoSync: true,
		model: 'submission',
		proxy: {
			type: 'rest',
			url: '" . base_path() . current_path() . "',
			extraParams: {'_getdata':'true'},
			reader: {
				type: 'json',
				root: 'results',
				totalProperty: 'total'
			},
			writer: {
				type: 'json',
				writeAllFields: false
			},
			listeners:{
				exception:function(proxy,response) {
					store.rejectChanges();
					var toolbar = Ext.getCmp('pagingToolbar');
					toolbar.displayMsg = '';
					toolbar.onLoad();
					assessDelete();
					var json = Ext.decode(response.responseText);
					if(json.message) {
						Ext.getCmp('errorMsg').setText(json.message);
					} else {
						Ext.getCmp('errorMsg').setText('Unknown error has occurred.  Try refreshing page.');
					}
				}
			}
		},
		listeners: {
			load: function(str, records, successful) {
				recordsToDelete = {};
				recordsToDeleteSize = 0;
				updateDeletes();
			},
			remove: function(str,record,opt) {
				delete recordsToDelete[record.getId()];
				--recordsToDeleteSize;
				updateDeletes();
				store.totalCount--;
				store.fireEvent('totalcountchange', store.totalCount);
				Ext.getCmp('pagingToolbar').onLoad();
			}
		},
		remoteSort: true,
		sorters: [{
			property: 'sid',
			direction: 'DESC'
		}]
	});
    var onDeleteSubmission = function() {
    	var records = [];
    	for(var key in recordsToDelete) {
    		records.push(recordsToDelete[key]);
    	}
    	store.remove(records);
	};
    var onDownload = function() {
    	var url = store.getProxy().url + '?' + '_getdata=excel';
    	window.open(url,'_blank');
	};
	var grid = Ext.create('Ext.grid.Panel', {
		store: store,
		plugins: [
			Ext.create('Ext.grid.plugin.CellEditing', {
				clicksToEdit: 2
			})
		],
		selType: 'rowmodel',
		columns: " . replaceFields(replaceStores(json_encode($columns),count($columns)),$fields) . ",
		dockedItems: [{
			xtype: 'toolbar',
			items: [{
				iconCls: 'delete-icon',
				text: 'Delete',
				disabled: true,
				id: 'deleteButton',";
	if(!$deleteAccess) { 
		$js .= "
				hidden: true
			},{";
	} else {
		$js .= "
				handler: onDeleteSubmission
			},'-',{";
	}
	$js .= "
				xtype: 'tbtext',
				id: 'errorMsg',
				text: ''
			},'->','-',{
				iconCls: 'excel-icon',
				text: 'Download',
				disabled: false,
				id: 'downloadButton',
				handler: onDownload
			}]
		},{
			xtype: 'pagingtoolbar',
			store: store,
			id: 'pagingToolbar',
			dock: 'bottom',
			displayMsg: 'Displaying submissions {0} - {1} of {2}',
			items: [
			'-',{ xtype:'tbspacer' },{
				xtype:'combobox',
				id:'submissions-per-page-combobox',
				fieldLabel:'Submissions<br/>per page',
				store: Ext.create('Ext.data.Store', {
				    fields: ['num'],
				    data : [";
	$firstPagerCount = true;
	foreach($possiblePagerCounts as $num) {
		$js .= ($firstPagerCount ? "" : ",") . "
						{num: " . $num . "}";
		$firstPagerCount = false;
	}
	$js .= "
				    ],
				}),
				labelWidth: 60,
				width:120,
				editable:false,
				valueField:'num',
				displayField:'num',
				queryMode:'local',
				value: " . $pager_count . ",
				listeners: { change: function(combo,newValue,oldValue) {
					Ext.getCmp('pagingToolbar').pageSize = newValue;
					store.pageSize = newValue;
					store.load();
				}}
			}],
			displayInfo: true
		}]
	});
	";
		if(count($validateDates) > 0 || count($validateTimes) > 0) {
	$js .= "
	grid.on('validateedit',function(ed,e) {";
			if(count($validateDates) > 0) { $js .= "
		if(Ext.Array.indexOf(" . json_encode($validateDates) . ", e.field) > -1 && typeof e.value != 'string') {
			e.cancel = true;
			grid.getSelectionModel().selected.first().set(e.field,Ext.Date.format(e.value,'Y-m-d'));
			return;
		}";
			}
			if(count($validateTimes) > 0) { $js .= "
		if(Ext.Array.indexOf(" . json_encode($validateTimes) . ", e.field) > -1 && typeof e.value != 'string') {
			e.cancel = true;
			grid.getSelectionModel().selected.first().set(e.field,Ext.Date.format(e.value,'H:i:s'));
			return;
		}";
			}
	$js .= "			
	});";
		}
	$js .= "
	var cont = Ext.create('Ext.container.Container', {
		renderTo: 'extjs-grid-container',
		layout: 'fit',
		listeners: { beforerender: function() {
			this.setHeight(Ext.getBody().getViewSize().height - Ext.get('content').getY() - 75);
		}},
		items: [grid]
	});
	
	Ext.EventManager.onWindowResize(function() {
		this.setHeight(Ext.getBody().getViewSize().height - 200);
	}, cont);
	
	Ext.getBody().removeCls('x-body');
	Ext.get('extjs-grid-container').addCls('x-body');
});	
	";
	
		drupal_add_js($js, array('type' => 'inline', 'scope' => 'header', 'weight' => 10000));
	
	}
	$output = '<div id="extjs-grid-container"></div>';

	return $output;
}

function replaceStores($jsonText, $columnCount) {
	$jsonText = str_replace("\"dateRenderer\"","dateRenderer",$jsonText);
	$jsonText = str_replace("\"sidRenderer\"","sidRenderer",$jsonText);
	$jsonText = str_replace("\"deleteSelectionListener\"","deleteSelectionListener",$jsonText);
	$jsonText = str_replace("\"readOnlyRenderer\"","readOnlyRenderer",$jsonText);
	$jsonText = str_replace("\"readOnlyWithEncodeRenderer\"","readOnlyWithEncodeRenderer",$jsonText);
	$jsonText = str_replace("\"htmlRenderer\"","Ext.util.Format.htmlEncode",$jsonText);
	$jsonText = str_replace("\"comboEmptyCheck\"","{'select': comboEmptyCheck,'focus': comboEmptyCheckFocus}",$jsonText);
	for($i=0; $i<$columnCount; $i++) {
		$jsonText = str_replace("\"store_" . $i . "\"","store_" . $i,$jsonText);
		$jsonText = str_replace("\"comboRenderer_" . $i . "\"","comboRenderer(store_" . $i . ")",$jsonText);
	}
	return $jsonText;
}

function replaceFields($jsonText, $fields) {
	for($i=0; $i<count($fields); $i++) {
		$jsonText = str_replace("\"multiselectListener_" . $fields[$i]['name'] . "\"","{'focus':multiselectListener('" . $fields[$i]['name'] . "')}",$jsonText);
	}
	return $jsonText;
}

/**
 * Rewriting this function (webform_submission_access) to account for null $submission
 */
function _webform_extended_table_webform_submission_access($node, $submission, $op = 'view', $account = NULL) {
  global $user;
  $account = isset($account) ? $account : $user;

  $access_all = user_access('access all webform results', $account);
  $access_own_submission = isset($submission) && user_access('access own webform submissions', $account) && (($account->uid && $account->uid == $submission->uid) || isset($_SESSION['webform_submission'][$submission->sid]));
  $access_node_submissions = user_access('access own webform results', $account) && $account->uid == $node->uid;

  $general_access = $access_all || $access_own_submission || $access_node_submissions;

  // Disable the page cache for anonymous users in this access callback,
  // otherwise the "Access denied" page gets cached.
  if (!$account->uid && user_access('access own webform submissions', $account)) {
    webform_disable_page_cache();
  }

  $module_access = count(array_filter(module_invoke_all('webform_submission_access', $node, $submission, $op, $account))) > 0;

  switch ($op) {
    case 'view':
      return $module_access || $general_access;
    case 'edit':  // Modified following line
      return $module_access || ($general_access && (user_access('edit all webform submissions', $account) || (user_access('edit own webform submissions', $account) && (isset($submission->uid) && $account->uid == $submission->uid))));
    case 'delete':  // Modified following line
      return $module_access || ($general_access && (user_access('delete all webform submissions', $account) || (user_access('delete own webform submissions', $account) && (isset($submission->uid) && $account->uid == $submission->uid))));
    case 'list':
      return $module_access || user_access('access all webform results', $account) || (user_access('access own webform submissions', $account) && ($account->uid || isset($_SESSION['webform_submission']))) || (user_access('access own webform results', $account) && $account->uid == $node->uid);
  }
}
